
# `ipyauth` Javascript part

Check out the [first-widget](https://github.com/ocoudray/first-widget) for more info.  

## 1 - Prerequisite

Install [node](http://nodejs.org/).  

## 2 - Install

```bash
npm install --save ipyauth
```
