# Auth0 Demo API

This demo API is useful for the Auth0 example.  
See the [doc page](https://oscar6echo.gitlab.io/ipyauth/guide/Auth0.html#demo-api).
